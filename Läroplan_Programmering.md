# Läroplan

## Konkret
### 1. Typer och operatorer

### 2. Variabler och konstanter


### 3. Kontrollstrukturer

### 4. Funktioner

### 5. Namngivningsstandarder

### 6. Listor
  * Listtyper

    * Arrays
    * Hashar / Associativa arrayer / Vektorer
    * Andra typer av listor, i korthet

  * Listbehandling

      * Mappning
      * Filtrering
      * Sortering

### 7. Felhantering (Error, exception, try/catch)

### 8. Objektorienterad programmering + PHP

  * Klasser: Deklarera, instantiera, abstrakta Klasser
  * Namespaces
  * Sammansättningar

    * Association
    * Arv
    * Aggregat
    * Komposition

  * Interfaces
  * Traits
  * Dependency Injection


## Abstrakt

### 1. Immutability och rena Funktioner

### 2. Designmönster (MVC)

### 3. SOLID

### 4. Kanske något mer, kanske något annat, vem vet!
